# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: finder version\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-12 17:17+0100\n"
"PO-Revision-Date: 2023-11-12 17:13+0100\n"
"Last-Translator: \n"
"Language-Team: antiX Team\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"


# Do not translate “yad” as it is the name of a program.
#: finder:13
msgid ""
"You need to have yad installed. Please make sure it is installed and re-run "
"this script"
msgstr ""

#: finder:14
msgid "Nothing to do"
msgstr ""

# Window title. You can add a small comment in the title after the name of the “Finder” program. For example, “Looking for something...”
#: finder:15
msgid "Finder"
msgstr ""

#: finder:16
msgid "not empty, continuing"
msgstr ""

#: finder:17
msgid ""
"No results found for files/folders with names containing this expression, in "
msgstr ""

# This is the text written in a button.
#: finder:18
msgid "Exit"
msgstr ""

#: finder:19
msgid "COMMAND could not be found"
msgstr ""

#: finder:20
msgid "is a command meant to be run from the terminal"
msgstr ""

#: finder:21
msgid ""
"There are instalable packages that match that query, available in the "
"Repository (and their version): "
msgstr ""

#: finder:22
msgid "No results found for this application or setting:"
msgstr ""

#: finder:23
msgid "Internet: ok"
msgstr ""

# “ddgr” is a package / program
#: finder:24
msgid "Download ddgr (40kb)?"
msgstr ""

# This is the text written in a button. Spaces before and after “OK” are not required if you are translating the abbreviation “OK”.
#: finder:25
msgid " OK "
msgstr ""

# “ddgr” is a package
#: finder:26
msgid "ddgr not installed!"
msgstr ""

#: finder:27
msgid "Search parameter is empty, exiting"
msgstr ""

#: finder:28
msgid "No region (results are shown in English)"
msgstr ""

#: finder:29
msgid "The current Region code for web searches is:"
msgstr ""

#: finder:30
msgid "It will be replaced by:"
msgstr ""

#: finder:31
msgid "starting Finder"
msgstr ""

# This is the text written in a button.
#: finder:32
msgid "Language"
msgstr ""

# This is the text written in a button.
#: finder:33
msgid "New search"
msgstr ""

# This is the text written in a button.
#: finder:34
msgid "Files"
msgstr ""

# This is the text written in a button.
#: finder:35
msgid "Apps/Settings"
msgstr ""

# This is the text written in a button.
#: finder:36
msgid "Web"
msgstr ""

# This is the text written in a button.
#: finder:37
msgid "Run"
msgstr ""

#: finder:38
msgid "(Terminal command)"
msgstr ""

# This means that the country, and therefore the language linked to the country, has not been configured to display search results in your language. By default, the results will be in English. The translation must be short!
#: finder:39
msgid "No region"
msgstr ""

# Never translate “$query”!
#: finder:40
#, sh-format
msgid ""
"WARNING: Closing this terminal window will close also the $query instance "
"running inside it!"
msgstr ""

#: finder:41
msgid "Press any key to close this terminal window"
msgstr ""

#: finder:42
msgid "Searching, please wait"
msgstr ""
